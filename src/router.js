import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from './views/Login.vue'
import Registration from './views/Registration.vue'
import Chat from './views/Chat.vue'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'Login',
            component: Login
        },
        {
            path: '/registration',
            name: 'Registration',
            component: Registration
        },
        {
            path: '/chat',
            name: 'Chat',
            component: Chat
        },
    ]
})
