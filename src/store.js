import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: {
            name: "",
            email: "",
            _id: ""
        }
    },
    mutations: {
        setUser(state, user) {
            state.user = { 
                name: user.name,
                email: user.email,
                _id: user._id
             };
        }
    },
    getters: {
        getUser(state) {
            return state.user;
        }
    }
})